

export default function NotFound(){
  return (
    <div style={{ marginTop: '89px' }}>
        <h1>404</h1>
        <p>Not found</p>
    </div>
  )
}