import DetailPage from '@/components/pages/works/detail'
import { works } from '@/components/pages/works/works'
import { notFound } from 'next/navigation'


export default function WorkDetailPage({ params }: { params: { id: string } }){

  const work = works.find(v => v.id === params.id)
  if(!work) notFound()

  return work ? <DetailPage work={work} /> : null

}