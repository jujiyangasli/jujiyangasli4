import type { Metadata } from 'next'
import 'sanitize.css'
import '@/styles/globals.css'

import Footer from '@/components/footer'
import Layout from '@/components/layout'
import Header from '@/components/header'
import Pendulum from '@/components/pendulum'

import { Open_Sans } from 'next/font/google'
const openSans = Open_Sans({
  subsets: ['latin'],
  weight: '400'
})

export const metadata: Metadata = {
  title: {
    template: '%s | Juji: Web Developer',
    default: 'Juji: Web Developer'
  },
  description: 'Hello, my name is Tri Rahmat Gunadi, and I like to code stuff',
  manifest: '/manifest.json?v=1',
  themeColor: '#111111',
  viewport: {
    width: 'device-width',
    initialScale: 1
  },
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      </head>
      <body className={openSans.className}>
        <Pendulum />
        <Header />
        <main>
          <Layout>
            {children}
          </Layout>
        </main>
        <Footer />
      </body>
    </html>
  )
}
