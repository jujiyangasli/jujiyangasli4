import Hello from "@/components/pages/hello"
import Works from "@/components/pages/works"
import Techs from "@/components/pages/techs"
import Contact from "@/components/pages/contact"

export default function Home() {
  return (<>
    <Hello />
    <Works />
    <Techs />
    <Contact />
  </>)
}
