'use client'

/*
useIsHome detects home by analyzing scroll
*/


import { useState, useEffect } from 'react'
import { usePathname } from 'next/navigation'

const TOP_SCROLL_LIMIT = 50

export default function useIsHome(){
  
  const pathname = usePathname()
  const isHomePage = pathname === '/'
  const [ isHome, setIsHome ] = useState(false)
  const haveWindow = typeof window !== 'undefined'

  // handle scroll event
  useEffect(() => {

    if(!haveWindow) return () => {};

    const scrollHandler = () => {
      if(!isHomePage && isHome) setIsHome(false);
      else if(!isHomePage && !isHome) return;
      else if (!isHome && window.scrollY <= TOP_SCROLL_LIMIT)
        setIsHome(true);
      
      else if (isHome && window.scrollY > TOP_SCROLL_LIMIT)
        setIsHome(false);
    }

    // init call
    // console.log('isHomePage', isHomePage)
    isHomePage && scrollHandler()

    window.addEventListener('scroll', scrollHandler)
    return () => {
      window.removeEventListener('scroll', scrollHandler)
    }

  },[ isHome, setIsHome, haveWindow, isHomePage, pathname ])

  return isHome

}