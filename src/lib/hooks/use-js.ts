'use client'

import { useEffect, useState } from "react"

/* set a state when js is enabled */

export default function useJs(){

  const [ jsEnabled, set ] = useState(false)

  useEffect(() => {
    set(typeof window !== 'undefined')
  },[])

  return jsEnabled

}