import { PropsWithChildren } from 'react'
import styles from './layout.module.scss'

export default function Layout({ children, className }: React.ComponentPropsWithoutRef<"div">){

  return <div className={`${className?className:''} ${styles.layout}`}>
    {children}
  </div>

}