'use client'

import styles from './works.module.scss'
import WorkThumbnail from './thumbnail'
import { works } from './works'
import { useMemo, useRef, useState } from 'react'
import { useTrackVisibility } from 'react-intersection-observer-hook'
import useJs from "@/lib/hooks/use-js";

export default function Works(){

  let animationDelay = 0
  const jsEnabled = useJs()
  const [ref, { isVisible, wasEverVisible }] = useTrackVisibility({
    rootMargin: '0% 0% -30% 0%'
  });

  // if this is visible, don't animate
  const [ paused, setPaused ] = useState(false)
  const timeout = useRef<ReturnType<typeof setTimeout>|null>(null)
  useMemo(() => {
    if(timeout.current) clearTimeout(timeout.current)
    timeout.current = setTimeout(() => {
      if(jsEnabled && !isVisible) setPaused(true)
    },1000)
  },[ isVisible, setPaused, jsEnabled ])

  return <div ref={ref} className={`
    ${styles.works}
    ${paused?styles.paused:''}
    ${wasEverVisible?styles.visible:''}
  `} id="works">

    <h1 className={`${styles.h1}`}>Works</h1>
    <p className={`${styles.par}`}>Things i did</p>

    <div className={styles.workThumbnails}>
      {works.map(work => <WorkThumbnail 
        animationDelay={1000 + ((animationDelay++) * 100)}
        jsEnabled={paused}
        wasEverVisible={wasEverVisible}
        key={`work.${work.id}`}
        work={work} />)}
    </div>
  </div>

}