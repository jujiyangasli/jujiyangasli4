'use client'

import styles from './detail.module.scss'
import { type Work } from '../works'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/navigation'

export default function DetailPage({ work }:{ work: Work }){

  const { 
    id,
    title,
    // logo,
    images
  } = work || {}

  const href = `/#works/${id}`;

  return <div className={styles.detail}>

    <br />
    <Link href={href}>back</Link>
    
    <div>
      <Image src={images[0].thumbnail}
        alt={title}
        width={images[0].dimension.thumb.width}
        height={images[0].dimension.thumb.height} />
    </div>

    
  </div>

}