'use client'

import VanillaTilt from "vanilla-tilt";
import styles from './thumbnail.module.scss'
import { useRef, useState } from "react";
import { type Work } from '../works'
import Image from "next/image";
import { useRouter } from "next/navigation";
import Link from 'next/link'

const options = {
  reverse: false,
  scale: 1.08,
  speed: 700,
  max: 14,
  glare: true,
  "max-glare": 0.6,
  gyroscope: true,
  gyroscopeMinAngleX: -36000,
  gyroscopeMaxAngleX: 36000,
  gyroscopeMinAngleY: -36000,
  gyroscopeMaxAngleY: 36000
};

export default function WorkThumbnail({ 
  work,
  animationDelay, 
  jsEnabled,
  wasEverVisible,
}:{ 
  work: Work 
  animationDelay: number
  jsEnabled: boolean
  wasEverVisible: boolean
}){

  const tilt = useRef<HTMLAnchorElement|null>(null)
  const router = useRouter();
  const imageRef = useRef<HTMLImageElement|null>(null)
  const [viewTransitionName, setViewTransitionName] = useState<string|null>(null)

  const { 
    id,
    title,
    logo,
    images
  } = work

  const href = `/works/${id}`;

  return <div 
    className={`
      ${styles.container}
      ${jsEnabled?styles.paused:''}
      ${wasEverVisible?styles.visible:''}
    `}
    style={{ animationDelay: `${animationDelay}ms` }}
  >
    <Link href={href} ref={ref => {
    tilt.current = ref as HTMLAnchorElement
    VanillaTilt.init(tilt.current, options);
  }}
    className={`${styles.workThumbnail}`} 
      title={title} 
      data-id={id}
      id={`works/${id}`}
    >
      <Image ref={imageRef} 
        src={images[0].thumbnail}
        alt={title}
        priority={true}
        width={images[0].dimension.thumb.width}
        height={images[0].dimension.thumb.height} />
      <span className={styles.shadow}>
        <Image src={logo.url}
          alt={title}
          width={logo.width}
          height={logo.height} />
      </span>
      <span className={styles.label}>
        <Image src={logo.url}
          alt={title}
          width={logo.width}
          height={logo.height} />
      </span>
    </Link>
  </div>

}