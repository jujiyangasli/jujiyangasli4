import { MenuList } from '@/lib/menu'
import Link from 'next/link'
import styles from './hello.module.scss'

export default function Menu(){

  return <div className={styles.menu}>
    {MenuList.map((menu, i) => <Link 
      key={`menu${i}`}
      data-animation-id={menu.id}
      href={menu.href}>{menu.text}</Link>)}
  </div>

}