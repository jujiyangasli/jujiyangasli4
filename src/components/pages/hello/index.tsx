import styles from './hello.module.scss'
import { Source_Code_Pro } from 'next/font/google'
import Menu from './menu'

const sourceCode = Source_Code_Pro({
  subsets: ['latin'],
  weight: '400'
})

export default function Hello(){

  return <div className={styles.hello} id="home">
    <p className={`${sourceCode.className} ${styles.hellobig}`}>Hello,</p>
    <h1>
      <span className={styles.p1}>My name is <b>Tri Rahmat Gunadi</b>,</span>
      <span className={styles.p2}>but people call me <a 
          title="Github Link"
          rel='noreferrer noopener'
          target="_blank"
          href='https://github.com/juji'
          className={styles.juji}>juji</a>.</span>
      <span className={styles.p3}>I am a web developer.</span>
    </h1>
    <br />
    <div id="smiley" className={styles.smiley}>;)</div>
    <Menu />
  </div>

}