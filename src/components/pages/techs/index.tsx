'use client'

import styles from './tech.module.scss'
import { tech } from './data'
import TechCell from './tech-cell'

import { useTrackVisibility } from 'react-intersection-observer-hook'
import useJs from "@/lib/hooks/use-js";


export default function Tech(){

    
    // https://dev.to/_hariti/master-the-rootmargin-in-intersection-observer-b3d
    const [ref, { wasEverVisible }] = useTrackVisibility({
        rootMargin: '0% 0% -30% 0%'
    });
    
    const jsEnabled = useJs()
    let animationDelay = 0
    
    return <div ref={ref} className={`${styles.tech} 
        ${wasEverVisible?styles.visible:''}
        ${jsEnabled?styles.paused:''}
    `} id="techs">

        <h1 className={`${styles.h1}`}>Technologies</h1>
        <p className={`${styles.par}`}>Techs i&apos;m comfortable with</p>

        { tech.map((group, i) => {
            
            
            return <div key={`techRow${i}`} className={styles.techRow}>

                {group.map((tech, idx) => {

                    return <TechCell
                        key={`techCell.${i}.${idx}`}
                        item={tech}
                        animationDelay={300 + ((animationDelay++) * 50)}
                        parentVisible={wasEverVisible}
                        jsEnabled={jsEnabled}
                    />

                })}

            </div>

        })}

    </div>

}