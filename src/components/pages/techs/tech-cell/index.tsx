import styles from './techcell.module.scss'
import Image from 'next/image';

import type { TechItem } from '../data'

export default function TechCell({ 
    item,
    animationDelay,
    parentVisible,
    jsEnabled 
}:{ 
    item: TechItem 
    parentVisible: boolean
    jsEnabled: boolean
    animationDelay: number
}){
    const { id, title, image, url, width, height } = item

    return <a href={url}
        data-id={id}
        className={`
            ${styles.cell} 
            ${jsEnabled?styles.anim:''} 
            ${parentVisible?styles.visible:''} 
            noline
        `.replace(/\r|\n/g,'').replace(/\s+/g,' ')}
        style={{ animationDelay: `${animationDelay}ms` }}
        title={title}
        aria-label={title} 
        target="_blank" rel="noopener noreferrer">
            <Image 
                src={image} 
                alt={title} 
                width={width}
                height={height}
            />
        </a>


}