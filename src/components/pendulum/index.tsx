'use client'

import { useState, useEffect, useRef, useMemo } from 'react'
import styles from './pendulum.module.scss'
import PendulumFn from './doublePendulum'

import useIsHome from '@/lib/hooks/use-is-home'
import useJs from '@/lib/hooks/use-js'

type PendulumImage = {
    src: string
    width: number
    height: number
}

export default function Pendulum(){

    const isHome = useIsHome()
    const [ img, setImage ] = useState<PendulumImage | null>(null)
    const [ started, setStarted ] = useState<number | null>(null)
    const timeout = useRef<ReturnType<typeof setTimeout> | null>()
    const startTime = useRef<number | null>()

    const haveWindow = typeof window !== 'undefined'

    const jsEnabled = useJs()

    // handle resize event
    useEffect(() => {

        if(!haveWindow) return () => {};

        const initWidth = window.innerWidth
        const handleResize = () => {
            
            // return when width are the same
            // resize may happen on height only
            if(window.innerWidth === initWidth) return;

            setImage(null)
            setStarted(null)
        }
        
        window.addEventListener('resize', handleResize)
        return () => {
            window.removeEventListener('resize', handleResize)
        }


    },[ haveWindow ])


    // delay draw start
    useEffect(() => {

        // first time, start immediately
        if(!started && !timeout.current) {
            
            startTime.current = new Date().valueOf()
            setStarted(startTime.current)
            timeout.current = setTimeout(() => {},0)
        
        // second time and so on
        }else if(timeout.current){
            
            clearTimeout(timeout.current)
            if(!started) timeout.current = setTimeout(() => {
                startTime.current = new Date().valueOf()
                setStarted(startTime.current)
            },500)

        }
    },[ started ])

    const windowInnerWidth = haveWindow && window.innerWidth || null

    const pendulumTimeout = useRef<ReturnType<typeof setTimeout>|null>(null)


    return <div className={`${styles.pendulum} ${!jsEnabled?styles.nojs:''}`} id="pendulum">

        
        <div className={`${styles.bg} ${isHome?'':styles.on}`}></div>
        <noscript>
            {/* eslint-disable-next-line @next/next/no-img-element */}
            <img className={styles.noscript} 
                src={'/images/pendulum.png'} 
                height={1440}
                width={911}
                alt="pendulum"
            />
        </noscript>

        
        <div className={styles.inside}>
            {/* eslint-disable-next-line @next/next/no-img-element */}
            { img ? <img 
                src={img.src} 
                height={img.height} 
                width={img.width} 
                alt="pendulum" /> : started ? (windowInnerWidth !== null ? <canvas 
                ref={canvasRef => {
                    if(pendulumTimeout.current) clearTimeout(pendulumTimeout.current)
                    pendulumTimeout.current = setTimeout(() => PendulumFn({
                        canvas: canvasRef,
                        started: started+'',
                        callback: (image: PendulumImage, ts: string) => {
                            // console.log('ts', ts)
                            // console.log('image', image)
                            ts === (started+'') && setImage(image)
                        }
                    }),1000)
                }}></canvas> : null) : null }
        </div>

        
    </div>

}