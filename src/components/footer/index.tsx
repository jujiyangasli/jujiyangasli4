import Link from 'next/link'
import styled from './footer.module.scss'
import Layout from '@/components/layout'

export default function Footer(){

  return <footer className={styled.footer}>
    <Layout>
      &copy; {new Date().getFullYear()} Tri Rahmat Gunadi -- <Link 
      className={styled.mailToLink}
      target="_blank" href="mailto:him@jujiyangasli.com">him@jujiyangasli.com</Link>
    </Layout>
  </footer>

}