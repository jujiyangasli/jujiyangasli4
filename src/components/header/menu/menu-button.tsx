import styles from './menu-button.module.scss'

export default function MenuButton({ 
  onClick,
  isOn,
  jsEnabled 
}:{
  isOn: boolean 
  jsEnabled: boolean
  onClick: () => void 
}){

  return <button
    title={isOn?"close menu":"open menu"}
    onClick={onClick} 
    className={`${styles.menuButton} ${isOn?styles.isOn:''} ${jsEnabled?styles.js:''}`}>
      <span></span>
      <span></span>
      <span></span>
  </button>

}