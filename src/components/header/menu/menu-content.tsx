import { MenuList } from '@/lib/menu'
import styles from './menu-content.module.scss'
import Link from 'next/link'

export default function MenuContent({
  isOn,
  onClick,
  jsEnabled
}:{
  isOn: boolean
  jsEnabled: boolean
  onClick: () => void
}){

  // non js IS NOT WORKING

  return <div className={`${styles.menuContent} ${!jsEnabled?styles.nojs:''} ${isOn?styles.isOn:''}`}>
    <div>
      <div>
        <h3>Menu</h3>
        <span data-animation-id={'home'}>
          <Link onClick={onClick} href={'/#home'}>Home</Link>
        </span>
        {MenuList.map((menu, i) => <span
          data-animation-id={menu.id}
          key={`menu-${menu.id}`}
        >
          <Link onClick={onClick} href={menu.href}>{menu.text}</Link>
        </span>)}
      </div>
    </div>
  </div>

}