'use client'

import { useEffect, useRef, useState } from 'react'
import MenuButton from './menu-button'
import MenuContent from './menu-content'
import styled from './menu.module.scss'
import useJs from '@/lib/hooks/use-js'

export default function Menu(){

  const [ isOn, setIsOn ] = useState(false)
  const onClickMenu = () => {
    setIsOn(!isOn)
  }

  const onClickItem = () => {
    setIsOn(false)
  }

  // const jsEnabled = false
  const jsEnabled = useJs()

  useEffect(() => {
    const html = document.querySelector('html')
    if(html && isOn) html.classList.add('menuOpen')
    if(html && !isOn) html.classList.remove('menuOpen')
  },[ isOn ])

  return <nav className={`${styled.menu} ${!jsEnabled?styled.nojs:''}`}>
    <MenuButton 
      jsEnabled={jsEnabled}
      isOn={isOn} 
      onClick={onClickMenu} />
    <MenuContent 
      jsEnabled={jsEnabled}
      isOn={isOn} 
      onClick={onClickItem} 
    />
  </nav>

}