'use client'

import Link from 'next/link'
import styled from './header.module.scss'
import Layout from '@/components/layout'
import { Source_Serif_4 } from 'next/font/google'
import useIsHome from '@/lib/hooks/use-is-home'
import Menu from './menu'

const sourceSerif = Source_Serif_4({
  subsets: ['latin'],
  weight: '400'
})

export default function Footer(){

  const isHome = useIsHome()

  return <header className={`${styled.header} ${isHome?'':styled.small}`}>
    <Layout className={styled.layout}>
      <Link className={`${sourceSerif.className} ${styled.logo}`} href="/#home">juji {'}'};</Link>
      <Menu />
    </Layout>
  </header>

}